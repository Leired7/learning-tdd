# TDD fizzbuzz

En esta dirección está la descripción de la tarea: (KatayunoApp)[http://katayuno-app.herokuapp.com/katas/1]

## Requerimientos

_Cinturón Amarillo_

[x] Listar los números del 1 al 100.

[x] Si el número es divisible entre 3, tiene que escribir "Fizz"

[x] Si el número es divisible entre 5, tiene que escribir "Buzz"

[x] Si el número es divisible entre 3 y 5, tiene que escribir "FizzBuzz"

[x] Si el número no es divisible ni entre 3 ni entre 5, tiene que escribir la representación del número.
