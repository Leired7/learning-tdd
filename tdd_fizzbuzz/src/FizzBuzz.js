const endNumner = 100;

function fizzBuzz(endNumner) {
  const result = [];

  for (let i = 0; i < endNumner; i++) {
    console.log(typeof i === "number");
    if (typeof i === "number" && i % 3 === 0 && i % 5 === 0) {
      result.push("FizzBuzz");
    } else if (typeof i === "number" && i % 3 === 0) {
      result.push("Fizz");
    } else if (typeof i === "number" && i % 5 === 0) {
      result.push("Buzz");
    } else {
      result.push(i);
    }
  }

  return result;
}
