describe("TDD learning with FizzBuzz Kata (static method)", () => {
  let endNumber;
  let result;

  beforeEach(() => {
    endNumber = 100;
    result = fizzBuzz(endNumber);
  });

  it("FizzBuzz functions should be truhy", () => {
    //Assert
    expect(fizzBuzz).toBeTruthy();
  });

  it("should list the numbers from 1 to 100", () => {
    //Assert
    expect(result.length).toBe(endNumber);
  });

  it('if the number is divisible by 3, write "Fizz', () => {
    const divisibleBy = 3;

    //assert
    expect(result[divisibleBy]).toBe("Fizz");
  });

  it('If the number is divisible by 5, write "Buzz" instead', () => {
    const divisibleBy = 5;

    //assert
    expect(result[divisibleBy]).toBe("Buzz");
  });

  it('If the number is divisible by both 3 and 5, write "FizzBuzz"', () => {
    const divisibleBy = 15;

    //Arrange
    expect(result[divisibleBy]).toBe("FizzBuzz");
  });
});

describe("TDD learning with FizzBuzz Kata (dinamic method)", () => {
  let endNumber;
  let result;

  beforeEach(() => {
    endNumber = 100;
    result = fizzBuzz(endNumber);
  });

  it('if the number is divisible by 3, write "Fizz', () => {
    //assert
    for (let i = 0; i < endNumber; i++) {
      if (
        typeof i === "number" &&
        result[i] !== "Buzz" &&
        result[i] !== "FizzBuzz" &&
        i % 3 === 0
      ) {
        expect(result[i]).toBe("Fizz");
      }
    }
  });

  it('If the number is divisible by 5, write "Buzz" instead', () => {
    //assert
    for (let i = 0; i < endNumber; i++) {
      if (
        typeof i === "number" &&
        result[i] !== "Fizz" &&
        result[i] !== "FizzBuzz" &&
        i % 5 === 0
      ) {
        expect(result[i]).toBe("Buzz");
      }
    }
  });

  it('If the number is divisible by both 3 and 5, write "FizzBuzz"', () => {
    //Arrange
    for (let i = 0; i < endNumber; i++) {
      if (typeof i === "number" && i % 3 === 0 && i % 5 === 0) {
        expect(result[i]).toBe("FizzBuzz");
      }
    }
  });
});
