const initCustomDelimeter = "//";

const add = (enterednumbersarray) => {
  const enteredStringValuesArray = enterednumbersarray.map((element) => {
    return parseInt(element);
  });

  const sumOfNumbers = enteredStringValuesArray.reduce((a, b) => a + b);

  return sumOfNumbers;
};

const isCustomParameterInTheString = (enteredStringValue) => {
  const enteredStringValueArray = enteredStringValue.split("");

  let a, b;

  const toValidateArray = ([a, b] = [
    enteredStringValueArray[0],
    enteredStringValueArray[1],
  ]);

  const toValidateString = toValidateArray.join();

  const toValidateStringRemoveComma = toValidateString.replace(",", "");

  if (toValidateStringRemoveComma === initCustomDelimeter) {
    const customParameter = enteredStringValue[2];

    const newStringOfNumbers = enteredStringValue.slice(4);

    return arrayOfNumbersToSum(newStringOfNumbers, customParameter);
  }

  return arrayOfNumbersToSum(enteredStringValue);
};

const arrayOfNumbersToSum = (enteredStringValue, customParameter) => {
  const commaParameter = ",";
  const newLineParameter = "\n";
  const parameterFromUser = customParameter;

  if (customParameter === undefined) {
    const enteredStringValuesArrayNewLine =
      enteredStringValue.split(newLineParameter);

    const enteredStringValuesArray = enteredStringValuesArrayNewLine
      .join()
      .split(commaParameter);

    return add(enteredStringValuesArray);
  } else {
    const a = enteredStringValue.split(parameterFromUser);

    return add(a);
  }
};

const StringCalculator = (enteredStringValue) => {
  return enteredStringValue.length === 0
    ? 0
    : enteredStringValue.length === 1
    ? parseInt(enteredStringValue)
    : isCustomParameterInTheString(enteredStringValue);
};
