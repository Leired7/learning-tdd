describe("TDD learning with String calculator kata", () => {
  it("should eit StringCalculator function", () => {
    expect(StringCalculator).toBeTruthy();
  });

  it('It has to return 0 if the entered value is ""', () => {
    //Arrange
    const enteredStringValue = "";

    //Act
    const result = StringCalculator(enteredStringValue);

    //Arrange
    expect(result).toBe(0);
  });

  it('It has to return 1 if the entered value is "1"', () => {
    //Arrange
    const enteredStringValue = "1";

    //Act
    const result = StringCalculator(enteredStringValue);

    //Arrange
    expect(result).toBe(1);
  });

  it('It has to return 2 if the entered value is "2"', () => {
    //Arrange
    const enteredStringValue = "2";

    //Act
    const result = StringCalculator(enteredStringValue);

    //Arrange
    expect(result).toBe(2);
  });

  it("It has to return the sum of numbers that are entered between commas", () => {
    //Arrange
    const enteredStringValue = "2,34";

    //Act
    const result = StringCalculator(enteredStringValue);

    //Arrange
    expect(result).toEqual(36);
  });

  it("It has to return any amount of numbers that are entered", () => {
    //Arrange
    const anotherenteredStringValue = "56,34,89,23";

    //Act
    const anotherResult = StringCalculator(anotherenteredStringValue);

    //Arrange
    expect(anotherResult).toEqual(202);
  });

  it("It has to handle new lines '\n' between numbers", () => {
    //Arrange
    const enteredStringValue = "1\n2,3";

    //Act
    const result = StringCalculator(enteredStringValue);

    //Arrange
    expect(result).toEqual(6);
  });

  it("It has to support different delimiters, like ';'", () => {
    //Arrange
    const enteredStringValue = "//:\n1:2";

    //Act
    const result = StringCalculator(enteredStringValue);

    //Arrange
    expect(result).toEqual(3);
  });

  xit("It has to throw an exception with negative numbers", () => {
    //Arrange
    const enteredStringValue = "1,4,-1";

    //Act
    const result = StringCalculator(enteredStringValue);

    //Arrange
    expect(result).toThrowError("hola");
  });
});
