# TDD String calculator

En esta dirección está la descripción de la tarea: (KatayunoApp)[http://katayuno-app.herokuapp.com/katas/5]

## Pasos

### 1

[x] La cadena de letras puede contener 0, 1 o 2 números, y tiene que devolver su suma

[x] Para cadenas vacías devolverá 0 y si hay una sola cadena devolverá ese número.

Consejos:

    - Empezar con el caso de test más simple de una cadena vacía, luego probar "1", luego dos números.

    - Recuerda resolver las cosas tan simple como sea posible para obligarme  a escribir pruebas en las que no has pensado

### 2

[x] Permitir que la calculadora trabaje con una cantidad desconocida de números.

### 3

[x] Permitir que la calculadora reconozca "\n" como separador de números en vez de comas.

    - **OK** `1\n2,3` resultado 6

    - **NOK** `1,\n` (no hace falta probarlo, solo por aclarar)

### 4

[x] Permitir que la calculadora soporte trabajar con diferentes delimitadores. Para cambiar el delimitador, el principio de la cadena tiene que contener "//[delimitador]\n[números...]"

    - Por ejemplo: `//;\n1;2` tiene que devolver 3 y su delimitador es ';'.

### 5

[] Llamara a add con un número negativo lanzará una excepción "Los números negativos no están permitidos" y el número negativo que se mando.

    - Por ejemplo: `add("1,4,-1")` lanzará una excepción con el mensaje "negativos no permitidos: -1".

[] Si hay varios negativos mostrarlos en el mensaje de la excepción.
