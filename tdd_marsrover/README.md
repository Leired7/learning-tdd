# TDD Mars Rover

En esta dirección está la descripción de la tarea: (KatayunoApp)[http://katayuno-app.herokuapp.com/katas/2]

## Requerimientos

_Cinturón amarillo_

[ ] El rover conoce su zona de aterrizaje.

    Código ejemplo:

    >
    Landing position: 5, 5, N
    Command: ""
    Final position: 5, 5, N

_Cinturón verde_

[ ] El rover conoce su zona posición final.

[ ] El rover recibe una lista de letras como comandos.

[ ] Cuando el rover recibe el comando "M" se mueve hacia adelante.

[ ] Cuando el rover recibe el comando "R" gira 90 grados a la derecha.

[ ] Cuando el rover recibe el comando "L" gira 90 grados a la izquierda.

    Código ejemplo:

    >
    Landing position: 1, 2, N
    Command: "MMM"
    Final position: 1, 5, N

_Cinturón rojo_

[ ] El rover debe conocer ahora las dimensiones del mundo.

[ ] Si el rover se pasa el borde del mundo, aparece posicionado en el lado opuesto (Los planetas son esféricos).

    Código ejemplo:
    >
    World dimensions: 5, 5
    Landing position: 1, 1, N
    Command: "MMMMM"
    Final position: 1, 1, N

_Cinturón negro_

[ ] No usar condicionales.

[ ] Usar métodos (no returns).

[ ] Aplicar solid a morir. (¿?)
